#!/usr/bin/env bash
# Copyright 2021 Nelson-Jean Gaasch
# Licence: (New BSD Licence)
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#######################
# version 1.0 - beta5 #
#######################
maximgwidth="1200"
maximgqual="80"
#######################
def='\033[0m'; red='\033[1;31m'; gre='\033[1;32m'; yel='\033[1;33m'
ssystem=$(uname)
if [[ "$ssystem" = "FreeBSD" ]]; then
	function f_keepsmallest {
		if [[ "$(stat -f %z "$1")" -le "$(stat -f %z "$2")" ]]; then rm "$2" && return 0;else return 1;fi
	}
elif [[ "$ssystem" = "Linux" ]]; then
	function f_keepsmallest {
		if [[ "$(stat --printf="%s" "$1")" -le "$(stat --printf="%s" "$2")" ]]; then rm "$2" && return 0;else return 1;fi
	}
fi

function f_optimg {
	du -h -d1
	printf "%b Patience, optimising images...%b\n" "$yel" "$def"
	if [[ ! -f "optimg.txt" ]]; then touch "optimg.txt"; fi
	optimlist=()&& while read -r imgnam; do optimlist+=("$imgnam"); done < "optimg.txt"
	for imgname in *.jpg *.jpeg *.png; do
		for i in "${optimlist[@]}"; do if [[ "$i" == "$imgname" ]]; then continue 2; fi; done
		ext=$(echo "$imgname"|rev|cut -f1 -d'.'|rev)
		filnam=$(echo "$imgname"|rev|cut -f2- -d'.'|rev)
		if [[ "$ext" = "jpeg" ]] && [[ "$filnam" != "*" ]]; then mv "$filnam.jpeg" "$filnam.jpg" && ext="jpg"; fi
		if [[ "$ext" != "png" ]] && [[ "$ext" != "jpg" ]]; then continue 2; fi
		printf "Optimising %b%s%b --> " "$red" "$imgname" "$def"
		width=$(identify -format '%w' "$filnam.$ext")
		if [[ "$width" -gt "$maximgwidth" ]]; then convert -resize "$maximgwidth" "$filnam.$ext" "$filnam.$ext.tmp"; fi
		if [[ -f "$filnam.$ext.tmp" ]]; then
			if ! f_keepsmallest "$filnam.$ext" "$filnam.$ext.tmp"; then
						mv "$filnam.$ext.tmp" "$filnam.$ext"
			fi
		fi
		if   [[ "$ext" = "jpg" ]]; then convert "$filnam.jpg" "$filnam.png"
		elif [[ "$ext" = "png" ]]; then
			if [[ "$(identify -format '%[channels]' "$filnam.png")" != "srgba" ]]; then #don't convert png that has alpha channel (transparency)
				convert "$filnam.png" "$filnam.jpg"
			fi
		fi
		pngquant -o "$filnam.png.mini" --force --quality="$maximgqual" "$filnam.png"
		if [[ -f "$filnam.png.mini" ]]; then
			if ! f_keepsmallest "$filnam.png" "$filnam.png.mini";
				then mv "$filnam.png.mini" "$filnam.png";
			fi
		fi
		if [[ -f "$filnam.jpg" ]]; then
			jpegoptim -q -m"$maximgqual" "$filnam.jpg"
			if ! f_keepsmallest "$filnam.png" "$filnam.jpg"; then ext="jpg" && rm "$filnam.png";else ext="png"; fi
		fi
		echo "$filnam.$ext" >> "optimg.txt"
		printf "%b%s.%s%b\n" "$gre" "$filnam" "$ext" "$def"
	done
du -h -d1
}
f_optimg
