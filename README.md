### Optimg ###

Tiny script to optimise all jpg/jpeg/png in a folder, based on size.

As I was rewriting my website for something lighter (wygibtwys) - I wanted to come up with a quick way if shrinking my images.
This is the function I wrote, taken out of it's "mother script".

# Should be working on FreeBSD and Linux #

Depends on Imagemagick, pngquant and jpegoptim

**It does not backup your files, use on a copy of your data**

## What is does? ##

1. for each jpg/jpeg/png in folder

2. Checks if file has been optimised already, if yes, proceed to next file

3. Resizes the file to $mawimgwidth

4. if jpg, makes a png copy

5. if png, makes a jpg copy only if no alpha channel

6. runs pngquant on png runs jpegoptim on jpeg

7. keep smallest file

**Consequence: files might change extension... better proceed before publishing online!**

### settings ###

# The max image width you want to allow (if image is bigger it will be reduced)

    maximgwidth="1200"

# The max image quality you want to allow (if quality is higher it will be reduced)

    maximgqual="80"

### Usage ###

    ./optimg.sh

It takes no arguments - _options are set with text editor in the script_ - it only works on the files that are in the directory it is in (no recursivity)!
